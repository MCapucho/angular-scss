import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SidenavService } from './sidenav/sidenav.service';

import { HeaderComponent } from './header/header.component';

import { CoreModule } from './../core/core.module';
import { MenuComponent } from './menu/menu.component';
import { SidenavComponent } from './sidenav/sidenav.component';

@NgModule({
  declarations: [
    HeaderComponent,
    MenuComponent,
    SidenavComponent
  ],

  imports: [
    CommonModule,
    CoreModule,
    RouterModule
  ],

  exports: [
    HeaderComponent,
    MenuComponent,
    SidenavComponent
  ],

  providers: [
    SidenavService
  ],

  entryComponents: [
    MenuComponent,
    SidenavComponent
  ]
})
export class TemplateModule { }
