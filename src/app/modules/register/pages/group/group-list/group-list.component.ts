import { GroupCreateComponent } from './../group-create/group-create.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';

import { Group } from './../../../models/group.model';

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.component.html',
  styleUrls: ['./group-list.component.scss']
})
export class GroupListComponent implements OnInit {

  displayedColumns = ['id', 'name'];
  dataSource = new MatTableDataSource<Group>();
  group: Group;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }

  openDialogCreate() {
    const dialogRef = this.dialog.open(GroupCreateComponent, {
      disableClose: true,
      width: '40%'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log(dialogRef.componentInstance.groupObject);
      }
    });
  }

}
