import { Group } from './../../../models/group.model';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-group-create',
  templateUrl: './group-create.component.html',
  styleUrls: ['./group-create.component.scss']
})
export class GroupCreateComponent implements OnInit {

  group: Group;

  groupObject = {} as Group;

  groupForm: FormGroup;

  closeDialog: boolean;
  editing: boolean;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.groupForm = this.formBuilder.group({
      name: this.formBuilder.control('', [Validators.required, Validators.minLength(3)])
    });
  }

  checkGroup() {
    if (this.group === undefined) {
      this.editing = false;
    } else {
      this.editing = true;
    }
  }

  clearGroup() {
    this.groupForm.reset();
  }

  createGroup() {
    this.groupObject.name = this.groupForm.controls['name'].value;
    this.dialogClose();
  }

  editGroup() {

  }

  dialogClose() {
    this.closeDialog = true;
  }
}
