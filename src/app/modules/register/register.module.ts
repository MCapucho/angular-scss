import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CoreModule } from './../../shared/core/core.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterRoutingModule } from './register-routing.module';

import { ExerciseComponent } from './pages/exercise/exercise.component';
import { FilterComponent } from './components/filter/filter.component';
import { GroupCreateComponent } from './pages/group/group-create/group-create.component';
import { GroupListComponent } from './pages/group/group-list/group-list.component';
import { RepetitionComponent } from './pages/repetition/repetition.component';

@NgModule({
  declarations: [
    ExerciseComponent,
    FilterComponent,
    GroupCreateComponent,
    GroupListComponent,
    RepetitionComponent
  ],

  imports: [
    CommonModule,
    CoreModule,
    FormsModule,
    ReactiveFormsModule,
    RegisterRoutingModule
  ],

  exports: [
    ExerciseComponent,
    FilterComponent,
    GroupCreateComponent,
    GroupListComponent,
    RepetitionComponent
  ],

  entryComponents: [
    GroupCreateComponent
  ]
})
export class RegisterModule { }
