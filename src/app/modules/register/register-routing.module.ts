import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExerciseComponent } from './pages/exercise/exercise.component';
import { GroupListComponent } from './pages/group/group-list/group-list.component';
import { RepetitionComponent } from './pages/repetition/repetition.component';

const routes: Routes = [
  {
    path: 'exercise',
    component: ExerciseComponent
  },

  {
    path: 'group',
    component: GroupListComponent
  },

  {
    path: 'repetition',
    component: RepetitionComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterRoutingModule { }
