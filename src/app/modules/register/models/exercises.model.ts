import { Group } from './group.model';

export class Exercise {
  id?: number;
  name?: string;
  group?: Group;
}
